#!/bin/sh
# Outputs an index page, grouped by category, for the given recipes to
# stdout.
set -e

echo "---
title: Recipes
---
Welcome to my recipe list!"

awk '
  /#\+CATEGORY:/ {
    sub(/#\+CATEGORY: /, "")
    categories[FILENAME] = $0
  }
  /#\+TITLE:/ {
    sub(/#\+TITLE: /, "")
    titles[FILENAME] = $0
  }
  END {
    for (file in titles) {
      printf("%s\t%s\t%s\n", categories[file], titles[file], file)
    }
  }
' "$@" | sort | awk -F'\t' '{
  if ($1 != lastcat) {
    lastcat = $1
    printf("\n## %s\n\n", $1)
  }
  sub(/org$/, "html", $3)
  printf("- [%s](./%s)\n", $2, $3)
}'
