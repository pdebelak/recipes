RECIPES=$(filter-out README.org template.org,$(wildcard *.org))
RENDERED_DIR=public
RENDERED=$(RECIPES:%.org=$(RENDERED_DIR)/%.html)
.PHONY: all clean server

all: $(RENDERED) $(RENDERED_DIR)/index.html

$(RENDERED_DIR)/index.html: $(RENDERED_DIR)/index.md $(RENDERED_DIR)
	pandoc $< -t html --self-contained -c recipe.css -H favicon.html -o $@

$(RENDERED_DIR)/index.md: $(RECIPES) create-index.sh $(RENDERED_DIR)
	sh create-index.sh $(RECIPES) >$@

$(RENDERED_DIR)/%.html: %.org $(RENDERED_DIR) recipe.css
	pandoc $< -t html --self-contained -c recipe.css -B header.html -H favicon.html -o $@

$(RENDERED_DIR):
	mkdir -p $@

%.org:
	cp template.org $@

server: all
	python3 -m http.server --directory public 8080

clean:
	rm -rf $(RENDERED_DIR)
